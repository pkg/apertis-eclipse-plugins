/*Copyright © 2016 Collabora Ltd.

SPDX-License-Identifier: MPL-2.0
This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

package org.apertis.plugin.jobs;

import java.io.File;
import java.io.IOException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;

import org.apertis.plugin.Constants;
import org.apertis.plugin.DownloadInterruptedException;
import org.apertis.plugin.InvalidSysrootException;
import org.apertis.plugin.Sysroot;
import org.apertis.plugin.ApertisPlugin;
import org.apertis.plugin.services.SysrootService;

public class SysrootUpdateJob extends Job {
	private Sysroot sysroot;

	public SysrootUpdateJob(String name, Sysroot sysroot) {
		super(name);
		this.sysroot = sysroot;
		setPriority(LONG);
	}

	@Override
	protected IStatus run(IProgressMonitor monitor) {
		SysrootService service = new SysrootService();
		
		monitor.beginTask(Constants.SYSROOT_UPDATE_JOB_BEGIN, 103);
		try {
			if (monitor.isCanceled()) {
				return Status.CANCEL_STATUS;
			}
			monitor.subTask(Constants.SYSROOT_UPDATE_JOB_DOWNLOAD);
			File archive = service.downloadSysroot(this.sysroot, monitor);
			if (monitor.isCanceled()) {
				return Status.CANCEL_STATUS;
			}
			monitor.subTask(Constants.SYSROOT_UPDATE_JOB_EXTRACT);
			service.extractArchive(this.sysroot, archive, monitor);
			monitor.worked(1);
			if (monitor.isCanceled()) {
				return Status.CANCEL_STATUS;
			}
			ApertisPlugin.getDefault().logInfo(Constants.SYSROOT_UPDATE_JOB_DONE);
			checkUpdateSuccessful();
			monitor.worked(1);
			
		} catch (InvalidSysrootException e) {
		} catch (IOException e) {
			ApertisPlugin.getDefault().logError(Constants.JOB_UPDATE_ERROR_MESSAGE, e);
			return Status.CANCEL_STATUS;
		} catch (DownloadInterruptedException e) {
			return Status.CANCEL_STATUS;
		} finally {
			monitor.done();
		}
		return Status.OK_STATUS;
	}
	
	public boolean belongsTo(Object family) {
        return family == Constants.APERTIS_JOB_FAMILY;
    }

	private void checkUpdateSuccessful() {
		/*ICheckUpdateService check = new CheckUpdateService();
		try {
			check.getSysrootVersion(sysroot.getPath().getAbsolutePath());
		} catch (IOException e) {
			ApertisPlugin.getDefault().logError(Constants.JOB_UPDATE_ERROR_MESSAGE, e);
		}*/
	}
}
