/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package org.apertis.plugin;

import org.eclipse.remote.core.IRemoteConnection;

public class RemoteHelper {
	public static String getDevice(IRemoteConnection connection) {
		String result = "";

		if (!connection.getUsername().isEmpty()) {
			result += connection.getUsername() + "@";
		}
		result += connection.getAddress();
		if (connection.getPort() != 22) {
			result += ":" + String.valueOf(connection.getPort());
		}

		return result;
	}
}
