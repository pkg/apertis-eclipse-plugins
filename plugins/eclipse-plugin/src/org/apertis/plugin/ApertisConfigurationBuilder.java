/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package org.apertis.plugin;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.cdt.core.CCorePlugin;
import org.eclipse.cdt.core.CommandLauncher;
import org.eclipse.cdt.core.model.CoreModel;
import org.eclipse.cdt.core.resources.ACBuilder;
import org.eclipse.cdt.core.resources.IConsole;
import org.eclipse.cdt.core.settings.model.ICProjectDescription;
import org.eclipse.cdt.managedbuilder.core.IConfiguration;
import org.eclipse.cdt.managedbuilder.core.IManagedBuildInfo;
import org.eclipse.cdt.managedbuilder.core.ManagedBuildManager;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.Path;

public class ApertisConfigurationBuilder extends ACBuilder {

	protected boolean isCdtProjectCreated(IProject project){
		ICProjectDescription des = CoreModel.getDefault().getProjectDescription(project, false);
		return des != null && !des.isCdtProjectCreating();
	}

	@Override
	protected IProject[] build(int kind, Map<String, String> args, IProgressMonitor monitor)
	throws CoreException {
		IProject project = getProject();
		if(!isCdtProjectCreated(project))
			return project.getReferencedProjects();

		IManagedBuildInfo info = ManagedBuildManager.getBuildInfo(project);
		MultiStatus result = performConfigure(project, info, args, monitor);
		if (result.getSeverity() == IStatus.ERROR) {
			// Failure to configure, output error message to console.
			IConsole console = CCorePlugin.getDefault().getConsole();
			console.start(project);

			OutputStream cos = console.getOutputStream();
			String errormsg = Constants.BUILD_STOPPED;
			StringBuilder buf = new StringBuilder(errormsg);
			buf.append(System.getProperty("line.separator", "\n")); //$NON-NLS-1$ //$NON-NLS-2$
			buf.append("(").append(result.getMessage()).append(")"); //$NON-NLS-1$ //$NON-NLS-2$

			try {
				cos.write(buf.toString().getBytes());
				cos.flush();
				cos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		checkCancel(monitor);
		return getProject().getReferencedProjects();
	}
	
	@Override
	protected void clean(IProgressMonitor monitor) {
		IProject project = getProject();
		final IManagedBuildInfo info = ManagedBuildManager.getBuildInfo(getProject());
		//TODO
	}

	protected MultiStatus performConfigure(IProject project, IManagedBuildInfo info,
			Map<String, String> args, IProgressMonitor monitor) {
		IConsole console = CCorePlugin.getDefault().getConsole("org.apertis.plugin.ui.apertisConsole");
		CommandLauncher launcher = new CommandLauncher();
		IPath location = project.getLocation();
		List<String> cmdList = new ArrayList<String>();
		MultiStatus result = null;

		if (monitor == null)
			monitor = new NullProgressMonitor();
		
		cmdList.add("--format=parseable");
		cmdList.add("configure");
		
		IConfiguration config = info.getDefaultConfiguration();
		if (config == null)
			return new MultiStatus(
					ApertisPlugin.getUniqueIdentifier(),
					IStatus.ERROR,
					"No selected configuration",
					null);

		/* FIXME Workaround to know which target we are building for */
		try {
			int lastDot = config.getId().lastIndexOf('.');
			int previousDot = config.getId().lastIndexOf('.', lastDot - 1);
			String target = config.getId().substring(previousDot + 1, lastDot);
			if (target.equals("simulator"))
				cmdList.add("--simulator");
			else if (target.equals("device"))
				cmdList.add("--sysroot=" + SysrootManager.getInstalled().getId());
			else
				return new MultiStatus(
						ApertisPlugin.getUniqueIdentifier(),
						IStatus.ERROR,
						"Invalid target",
						null);
			
			if (config.getId().contains("debug"))
			{
				cmdList.add("--debug");
			}
		} catch (InvalidSysrootException e) {
			return new MultiStatus(
					ApertisPlugin.getUniqueIdentifier(),
					IStatus.ERROR,
					"Invalid sysroot installed",
					e);
		}

		try {
			console.start(project);
			OutputStream stdout = console.getOutputStream();
			OutputStream stderr = stdout;
			
			String[] cmd = new String[cmdList.size()];
			cmdList.toArray (cmd);

			launcher.execute(new Path("/usr/bin/ade"), cmd, null, location, null);
			stdout.write(launcher.getCommandLine().getBytes());
			stdout.write('\n');
			if (launcher.waitAndRead(stdout, stderr, monitor) != CommandLauncher.OK) {
				throw new Exception(stderr.toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = new MultiStatus(ApertisPlugin.getUniqueIdentifier(), IStatus.ERROR,
					Constants.GENERATE_ERROR_MESSAGE, e);
		}
		
		if (result == null) {
			result = new MultiStatus(
					ApertisPlugin.getUniqueIdentifier(),
					IStatus.OK,
					"",
					null);
		}
		
		return result;
	}

	/**
	 * Check whether the build has been canceled.
	 */
	public void checkCancel(IProgressMonitor monitor) {
		if (monitor != null && monitor.isCanceled())
			throw new OperationCanceledException();
	}

}
