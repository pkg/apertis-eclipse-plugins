/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package org.apertis.plugin.ui;

import org.eclipse.cdt.internal.autotools.ui.Console;

@SuppressWarnings("restriction")
public class ApertisConsole extends Console {
	private static final String CONTEXT_MENU_ID = "ApertisConsole"; 
	private static final String CONSOLE_NAME = "Apertis Console";
	
	public ApertisConsole() {
		super(CONSOLE_NAME, CONTEXT_MENU_ID);
	}
}
