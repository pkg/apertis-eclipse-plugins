/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package org.apertis.plugin.jobs;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apertis.plugin.AdeHelper;
import org.apertis.plugin.ApertisPlugin;
import org.apertis.plugin.Constants;
import org.eclipse.cdt.core.CCorePlugin;
import org.eclipse.cdt.core.CommandLauncher;
import org.eclipse.cdt.core.resources.IConsole;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.jobs.Job;

public class AppDebugJob extends Job {
	protected static final long DELAY = 50L;

	private String device;
	private IProject project;
	private String exePath;
	private String arguments;

	private Process process;
	private String hostname;
	private int port;

	public AppDebugJob(String device, IProject project, String exePath, String arguments) {
		super("RemoteDebugJob");
		this.device = device;
		this.project = project;
		this.exePath = exePath;
		this.arguments = arguments;
	}

	public AppDebugJob(IProject project, String exePath, String arguments) {
		super("LocalDebugJob");
		this.device = null;
		this.project = project;
		this.exePath = exePath;
		this.arguments = arguments;
	}

	public String getHostname() {
		return this.hostname;
	}
	
	public int getPort() {
		return this.port;
	}
	
	public Process getProcess() {
		return this.process;
	}
	
	@SuppressWarnings("restriction")
	@Override
	protected IStatus run(IProgressMonitor monitor) {
		IConsole console = CCorePlugin.getDefault().getConsole("org.apertis.plugin.ui.apertisConsole");
		CommandLauncher launcher = new CommandLauncher();
		IPath location = this.project.getLocation();
		String[] args = new String[6];
		MultiStatus result = null;

		if (monitor == null)
			monitor = new NullProgressMonitor();

		args[0] = "--format=parseable";
		args[1] = "debug";
		args[2] = "--no-interactive";
		if (this.device != null) {
			args[3] = "--device=" + this.device;
		} else {
			args[3] = "--simulator";
		}
		args[4] = "--app=" + this.exePath;
		args[5] = this.arguments;
		
		/* FIXME: We need "Advanced Preferences" for the Apertis eclipse plugin
		 * to set environment variables.
		 * 
		 * https://phabricator.apertis.org/T3299
		 * 
		 * python uses buffered stdout by default so it can cause infinite waiting
		 * when BufferedReader tries to read strings. Rather than modifying 'ade'
		 * not to use buffered stdout (python -u option), this works around to make
		 * 'ade' print strings immediately.
		 * 
		 * The below iteration for all existing environment variables is required to
		 * preserve the values.
		 */
		List<String> envList = new ArrayList<String>();
		
		Properties envProps = launcher.getEnvironment();
		Enumeration<Object> enumProps = envProps.keys();
		
		while (enumProps.hasMoreElements()) {
			Object key = enumProps.nextElement();
			
			envList.add(key + "=" + envProps.get(key));
		}
		
		envList.add("PYTHONUNBUFFERED=1");
		
		try {
			console.start(this.project);
			
			BufferedWriter stdout = new BufferedWriter(new OutputStreamWriter(console.getOutputStream()));

			this.process = launcher.execute(new Path("ade"), args, envList.toArray(new String[envList.size()]), location, null);
			
			stdout.write(launcher.getCommandLine());
			stdout.newLine();
			stdout.flush();
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(this.process.getInputStream()));
			String line;
			while (!monitor.isCanceled() && (line = reader.readLine()) != null) {
				stdout.write(line);
				stdout.newLine();
				stdout.flush();
				
				Map<String, String> map = AdeHelper.parseOutput(line);
				if (map.containsKey("GdbServerHost")) {
					this.hostname = map.get("GdbServerHost");
				} else if (map.containsKey("GdbServerPort")) {
					this.port = Integer.parseInt(map.get("GdbServerPort"));
				} else if (map.containsKey("GdbServerReady")) {
					break;
				}
			}

			if (monitor.isCanceled()) {
				this.process.destroy();
				result = new MultiStatus(ApertisPlugin.getUniqueIdentifier(), IStatus.ERROR,
						Constants.CANCELED_ERROR_MESSAGE, null);
				try {
					this.process.waitFor();
				} catch (InterruptedException e) {
					// ignore
				}
			}
			
			stdout.close();
			
		} catch (Exception e) {
			e.printStackTrace();
			result = new MultiStatus(ApertisPlugin.getUniqueIdentifier(), IStatus.ERROR,
					Constants.DEBUG_ERROR_MESSAGE, e);
		}

		if (result == null) {
			result = new MultiStatus(ApertisPlugin.getUniqueIdentifier(), IStatus.OK, "", null);
		}

		return result;
	}

}
