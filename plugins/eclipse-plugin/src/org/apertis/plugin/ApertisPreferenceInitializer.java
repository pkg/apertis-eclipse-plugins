package org.apertis.plugin;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;

public class ApertisPreferenceInitializer extends AbstractPreferenceInitializer {

	public ApertisPreferenceInitializer() {
		super();
	}

	@Override
	public void initializeDefaultPreferences() {
		IPreferenceStore store = ApertisPlugin.getDefault().getPreferenceStore();
		store.setDefault(Constants.APERTIS_SYSROOT_UPDATE_USERNAME_PREFERENCE, "");
		store.setDefault(Constants.APERTIS_SYSROOT_UPDATE_PASSWORD_PREFERENCE, "");
		store.setDefault(Constants.APERTIS_SYSROOT_UPDATE_SSL_PREFERENCE, true);
	}

}
