/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package org.apertis.plugin;

import java.util.ArrayList;

import org.eclipse.cdt.managedbuilder.core.ManagedCProjectNature;
import org.eclipse.core.resources.ICommand;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IProjectNature;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceDescription;
import org.eclipse.core.resources.IWorkspaceRunnable;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.core.runtime.jobs.Job;

public class ApertisProjectNature implements IProjectNature {

	public static final String NATURE_ID = ApertisPlugin.getPluginId() + ".apertisNature";  //$NON-NLS-1$
	public static final String BUILDER_NAME = "adebuilder"; //$NON-NLS-1$
	public static final String BUILDER_ID = ApertisPlugin.getPluginId() + "." + BUILDER_NAME; //$NON-NLS-1$
	public static final String REMOTE_BUILDER_ID = "org.eclipse.ptp.rdt.sync.cdt.core.SyncBuilder"; //$NON-NLS-1$

	private IProject project;

	@Override
	public void configure() throws CoreException {
		addAdeBuilder(this.project, new NullProgressMonitor());
	}

	@Override
	public void deconfigure() throws CoreException {
		// TODO Remove builder
	}

	@Override
	public IProject getProject() {
		return this.project;
	}

	@Override
	public void setProject(IProject project) {
		this.project = project;
	}
	
	public static void addAdeBuilder(IProject project, IProgressMonitor monitor) throws CoreException {
		// Add the builder to the project
		IProjectDescription description = project.getDescription();
		ICommand[] commands = description.getBuildSpec();
		for (int j = 0; j < commands.length; ++j) {
			System.out.println(commands[j].getBuilderName());
		}
		if(checkEquals(commands,getBuildCommandsList(description, commands))){
			return;
		}

		final ISchedulingRule rule = ResourcesPlugin.getWorkspace().getRoot();
		final IProject proj = project;

		Job backgroundJob = new Job("Apertis Set Project Description") {
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				try {
					ResourcesPlugin.getWorkspace().run(new IWorkspaceRunnable() {
						protected boolean savedAutoBuildingValue;

						@Override
						public void run(IProgressMonitor monitor) throws CoreException {
							IWorkspace workspace = ResourcesPlugin.getWorkspace();
							turnOffAutoBuild(workspace);
							IProjectDescription prDescription = proj.getDescription();
							ICommand[] currentCommands = prDescription.getBuildSpec();
							ICommand[] newCommands = getBuildCommandsList(prDescription, currentCommands); 
							if(!checkEquals(currentCommands,newCommands)){
								prDescription.setBuildSpec(newCommands);
								proj.setDescription(prDescription, new NullProgressMonitor());
							}
							for (int j = 0; j < newCommands.length; ++j) {
								System.out.println(newCommands[j].getBuilderName());
							}
							restoreAutoBuild(workspace);
						}
						
						protected final void turnOffAutoBuild(IWorkspace workspace) throws CoreException {
							IWorkspaceDescription workspaceDesc = workspace.getDescription();
							savedAutoBuildingValue = workspaceDesc.isAutoBuilding();
							workspaceDesc.setAutoBuilding(false);
							workspace.setDescription(workspaceDesc);
						}
						
						protected final void restoreAutoBuild(IWorkspace workspace) throws CoreException {
							IWorkspaceDescription workspaceDesc = workspace.getDescription();
							workspaceDesc.setAutoBuilding(savedAutoBuildingValue);
							workspace.setDescription(workspaceDesc);
						}

					}, rule, IWorkspace.AVOID_UPDATE, monitor);
				} catch (CoreException e) {
					return e.getStatus();
				}
				return Status.OK_STATUS;
			}
		};
		backgroundJob.setRule(rule);
		backgroundJob.schedule();
	}

	static boolean checkEquals(ICommand[] commands,
			ICommand[] newCommands) {
			if (newCommands.length != commands.length){
				return false;
			}
			for (int j = 0; j < commands.length; ++j) {
				if (!commands[j].getBuilderName().equals(newCommands[j].getBuilderName())) {
					return false;
				}
			}
			return true;
	}

	static ICommand[] getBuildCommandsList(IProjectDescription description,
			ICommand[] commands) {
		ArrayList<ICommand> commandList = new ArrayList<ICommand>();

		for (int i = 0; i < commands.length; i++) {
			ICommand command = commands[i];
			if (command.getBuilderName().equals(BUILDER_ID)) {
				// ignore it
			} else {
				// Make sure that the Apertis builder precedes the Managed builder
				// or the Remote Synchronized builder.
				
				if (command.getBuilderName().equals(ManagedCProjectNature.BUILDER_ID) ||
						command.getBuilderName().equals(REMOTE_BUILDER_ID)) {
					ICommand newCommand = description.newCommand();
					newCommand.setBuilderName(BUILDER_ID);
					commandList.add(newCommand);
				}
				commandList.add(command);
			}
		}
		return commandList.toArray(new ICommand[commandList.size()]);
	}


}
