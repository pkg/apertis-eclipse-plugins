/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package org.apertis.plugin.ui;

import java.io.File;

import org.apertis.plugin.Constants;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Label;

public class BundleExportWizardPage extends WizardPage {
	IStructuredSelection selection;
	Label browseLabel;
	Combo browseCombo;
	Button browseBtn;

	protected BundleExportWizardPage(IStructuredSelection selection, String pageName) {
		super(pageName);
		this.selection = selection;
		setTitle(Constants.ExportWizard_dialog_title);
		setDescription(Constants.ExportWizard_dialog_message);
	}

	@Override
	public void createControl(Composite parent) {
		System.out.println("CREATING CONTROL");
		Composite composite = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		composite.setLayout(layout);
		composite.setLayoutData(new GridData(GridData.FILL_BOTH));

		browseLabel = new Label(composite, SWT.NONE);
		browseLabel.setText("Directory:");

		browseCombo = new Combo(composite, SWT.BORDER);
		browseCombo.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		browseCombo.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				setPageComplete(browseCombo.getText().trim().length() > 0);
			}
		});

		browseBtn = new Button(composite, SWT.PUSH);
		browseBtn.setText(Constants.ExportWizard_dialog_browse);
		browseBtn.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				chooseDestination();
			}
		});

		setControl(composite);
	}

	private void chooseDestination() {
		DirectoryDialog dialog = new DirectoryDialog(this.getShell(), SWT.SAVE);
		String path = browseCombo.getText();

		dialog.setFilterPath(path);
		dialog.setText(Constants.ExportWizard_dialog_title);
		dialog.setMessage(Constants.ExportWizard_dialog_message);
		String res = dialog.open();
		if (res != null) {
			if (browseCombo.indexOf(res) == -1)
				browseCombo.add(res, 0);
			browseCombo.setText(res);
		}
	}

	protected IProject getSelectedProject() {
		for (Object elem : this.selection.toArray()) {
			if (elem instanceof IFile) {
				IFile file = (IFile) elem;
				return file.getProject();
			} else if (elem instanceof IProject) {
				return (IProject) elem;
			}
		}
		return null;
	}

	protected String getDestination() {
		File dir = new File(browseCombo.getText().trim());
		return dir.getAbsolutePath();
	}

}
