/**
 * Copyright © 2017 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package org.apertis.plugin.ui.menu;

import org.apertis.plugin.ApertisPlugin;
import org.apertis.plugin.jobs.AppInstallJob;
import org.eclipse.cdt.launch.remote.IRemoteConnectionConfigurationConstants;
import org.eclipse.cdt.launch.remote.RSEHelper;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.rse.core.model.IHost;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

public class AdeCommandHandler extends AbstractHandler {

	final static String PARAM_ID_COMMAND = "org.apertis.plugin.ui.menu.AdeCommand.Command";
	final static String PARAM_ID_TARGET_OPTION = "org.apertis.plugin.ui.menu.AdeCommand.TargetOption";

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		Job job = null;
		String device = null;

		String adeCommand = event.getParameter(PARAM_ID_COMMAND);
		String targetOption = event.getParameter(PARAM_ID_TARGET_OPTION);

		IWorkbenchWindow window = HandlerUtil
				.getActiveWorkbenchWindowChecked(event);

		IStructuredSelection selection = (IStructuredSelection) window
				.getSelectionService().getSelection();
		IProject project = (IProject) selection.getFirstElement();

		ILaunchManager manager = DebugPlugin.getDefault().getLaunchManager();
		ILaunchConfigurationType type = manager
				.getLaunchConfigurationType(ApertisPlugin.ID_LAUNCH_CONFIGURATION);

		if ("device".equals(targetOption)) {
			String userId = null;
			String hostName = null;

			try {
				ILaunchConfiguration[] launchConfigs = manager
						.getLaunchConfigurations(type);

				if (launchConfigs.length < 1) {
					ApertisPlugin
							.getDefault()
							.logInfo(
									"Remote connection must be set in Run/Debug menu first.");
					return null;
				}
				String remoteConnection = launchConfigs[0]
						.getAttribute(
								IRemoteConnectionConfigurationConstants.ATTR_REMOTE_CONNECTION,
								""); //$NON-NLS-1$

				IHost host = RSEHelper
						.getRemoteConnectionByName(remoteConnection);

				if (!host.getDefaultUserId().isEmpty()) {
					userId = host.getDefaultUserId();
					hostName = host.getHostName();
				}
			} catch (CoreException e) {
				ApertisPlugin.getDefault().logError(
						"Can't get launch configurations.", e);

				return null;
			}

			if (userId == null || hostName == null) {
				/* Abort to launch command because of invalid device information */
				return null;
			}

			device = userId + "@" + hostName;
		}

		if ("install".equals(adeCommand)) {
			job = new AppInstallJob(device, project);
		} else if ("uninstall".equals(adeCommand)) {
			job = new AppInstallJob(device, project, false);
		}

		if (job != null) {
			job.schedule();
		}

		return null;
	}

}
