/*Copyright © 2016 Collabora Ltd.

SPDX-License-Identifier: MPL-2.0
This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package org.apertis.plugin.ui;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import org.apertis.plugin.Constants;
import org.apertis.plugin.InvalidSysrootException;
import org.apertis.plugin.Sysroot;
import org.apertis.plugin.services.SysrootService;
import org.apertis.plugin.ApertisPlugin;

public class SysrootUpdateDialog extends TitleAreaDialog { 

	private Text currentText;
	private Text availableText;
	private Sysroot currentSysroot;
	private Sysroot availableSysroot;
	private SysrootService service; 

	public SysrootUpdateDialog(Shell parentShell) {
		super(parentShell);
		service = new SysrootService();
	}

	@Override
	public void create() {
		super.create();
		// Set the title
		setTitle(Constants.CHECK_UPDATE_SYSROOT_TITLE);
		// Set the message
		setMessage(Constants.UPDATE_SYSROOT_SUBTITLE, 
				IMessageProvider.INFORMATION);

		getButton(IDialogConstants.OK_ID).setText(Constants.UPDATE_BUTTON_LABEL);
		getButton(IDialogConstants.OK_ID).setEnabled(false);
	}

	protected Control createDialogArea(Composite parent) {
		Composite group = new Composite(parent, SWT.NONE);
		group.setLayout(new GridLayout(2, false));
		GridData gd = new GridData(GridData.FILL_BOTH);
		gd.verticalAlignment = SWT.TOP;
		gd.grabExcessVerticalSpace = true;
		group.setLayoutData(gd);

		Label currentLabel = new Label(group, SWT.LEFT);
		currentLabel.setText(Constants.SYSROOT_CURRENT_VERSION_LABEL);

		// The text fields will grow with the size of the dialog
		currentText = new Text(group, SWT.LEFT | SWT.BORDER);
		GridData currentTextGrid = new GridData();
		currentTextGrid.grabExcessHorizontalSpace = true;
		currentTextGrid.horizontalAlignment = GridData.FILL;
		currentText.setLayoutData(currentTextGrid);
		currentText.setEnabled(false);
		currentText.setText(Constants.PENDING_LABEL);

		Label availableLabel = new Label(group, SWT.LEFT);
		availableLabel.setText(Constants.SYSROOT_AVAILABLE_VERSION_LABEL);

		// The text fields will grow with the size of the dialog
		availableText = new Text(group, SWT.LEFT | SWT.BORDER);
		GridData availableTextGrid = new GridData();
		availableTextGrid.grabExcessHorizontalSpace = true;
		availableTextGrid.horizontalAlignment = GridData.FILL;
		availableText.setLayoutData(availableTextGrid);
		availableText.setEnabled(false);
		availableText.setText(Constants.PENDING_LABEL);

		checkUpdate();

		return parent;
	}

	@Override
	protected void okPressed() {
		if (currentSysroot == null) {
			ApertisPlugin.getDefault().installSysroot(availableSysroot);
		} else if (availableSysroot.compareTo(currentSysroot) > 0) {
			ApertisPlugin.getDefault().updateSysroot(availableSysroot);
		}
		super.okPressed();
	}

	private void updateControls() {
		boolean installable = (currentSysroot == null ||
				(availableSysroot.isCompatible(currentSysroot) &&
						availableSysroot.compareTo(currentSysroot) > 0));

		if (currentSysroot == null) {
			currentText.setText(Constants.SYSROOT_NOT_INSTALLED);
		} else {
			currentText.setText(currentSysroot.toString());
		}
		if (availableSysroot == null) {
			setErrorMessage(Constants.UPDATE_VERSION_ERROR_MESSAGE);
		} else {
			availableText.setText(availableSysroot.toString());
		}

		if (currentSysroot != null && availableSysroot != null && 
				!currentSysroot.isCompatible(availableSysroot)) {
			setErrorMessage(Constants.UPDATE_VERSION_MISMATCH_ERROR_MESSAGE);
		}

		if (installable) {
			if (currentSysroot == null)
				getButton(IDialogConstants.OK_ID).setText(Constants.INSTALL_BUTTON_LABEL);
			else
				getButton(IDialogConstants.OK_ID).setText(Constants.UPDATE_BUTTON_LABEL);
			getButton(IDialogConstants.OK_ID).setEnabled(true);
		}
	}

	private void checkUpdate() {
		Job job = new Job(Constants.SYSROOT_CHECK_JOB) {
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				monitor.beginTask(Constants.SYSROOT_CHECK_JOB_NAME, 2);
				try {
					monitor.subTask(Constants.SYSROOT_CHECK_INSTALLED_JOB);
					currentSysroot = service.getInstalledVersion();
				} catch (InvalidSysrootException e) {
					e.printStackTrace();
				}
				monitor.worked(1);
				try {
					monitor.subTask(Constants.SYSROOT_CHECK_LATEST_JOB);
					availableSysroot = service.getLatestVersion();
				} catch (InvalidSysrootException e) {
					e.printStackTrace();
				}
				monitor.worked(1);
				monitor.done();

				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						updateControls();
					}
				});
				return Status.OK_STATUS;
			}
		};
		job.schedule();
	}
}
