/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package org.apertis.plugin;

import org.apertis.plugin.jobs.AppDebugJob;
import org.apertis.plugin.jobs.AppInstallJob;
import org.apertis.plugin.jobs.AppLaunchJob;
import org.eclipse.cdt.debug.core.ICDTLaunchConfigurationConstants;
import org.eclipse.cdt.dsf.gdb.IGDBLaunchConfigurationConstants;
import org.eclipse.cdt.dsf.gdb.launching.GdbLaunchDelegate;
import org.eclipse.cdt.launch.remote.IRemoteConnectionConfigurationConstants;
import org.eclipse.cdt.launch.remote.RSEHelper;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.rse.core.model.IHost;

public class RemoteLaunchDelegate extends GdbLaunchDelegate {

	@Override
	protected String getPluginID() {
		return ApertisPlugin.getPluginId();
	}

	@SuppressWarnings("restriction")
	public String getCurrentConnection(ILaunchConfiguration config)
			throws CoreException {
		String remoteConnection = config.getAttribute(
				IRemoteConnectionConfigurationConstants.ATTR_REMOTE_CONNECTION,
				""); //$NON-NLS-1$
		String device = "";
		IHost host = RSEHelper.getRemoteConnectionByName(remoteConnection);
		if (host == null) {
			abort(Constants.NO_CONNECTION_ERROR_MESSAGE, null, 150);
		}
		if (!host.getDefaultUserId().isEmpty())
			device += host.getDefaultUserId() + "@";
		device += host.getHostName();
		return device;
	}

	@Override
	public void launch(ILaunchConfiguration config, String mode,
			ILaunch launch, IProgressMonitor monitor) throws CoreException {
		IProject project = config.getMappedResources()[0].getProject();
		String device = getCurrentConnection(config);
		installToTarget(config, project, device);
		if (mode.equals(ILaunchManager.RUN_MODE)) {
			runOnTarget(config, project, device);
		} else if (mode.equals(ILaunchManager.DEBUG_MODE)) {
			debugOnTarget(config, launch, project, device);
		}

		try {
			super.launch(config, mode, launch, monitor);
		} catch (CoreException e) {

		} finally {
			monitor.done();
		}
	}

	@SuppressWarnings("restriction")
	protected void installToTarget(ILaunchConfiguration config,
			IProject project, String device) throws CoreException {
		boolean skipDownload = config
				.getAttribute(
						IRemoteConnectionConfigurationConstants.ATTR_SKIP_DOWNLOAD_TO_TARGET,
						false);

		if (skipDownload)
			return;

		Job job = new AppInstallJob(device, project);
		job.schedule();

		try {
			job.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	protected void runOnTarget(ILaunchConfiguration config, IProject project,
			String device) throws CoreException {
		String exePath = config.getAttribute(
				IRemoteConnectionConfigurationConstants.ATTR_REMOTE_PATH,
				(String) null);
		String arguments = config.getAttribute(
				ICDTLaunchConfigurationConstants.ATTR_PROGRAM_ARGUMENTS, "");

		Job job = new AppLaunchJob(device, project, exePath, arguments);
		job.schedule();
		try {
			job.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	protected void debugOnTarget(ILaunchConfiguration config, ILaunch launch,
			IProject project, String device) throws CoreException {
		String exePath = config.getAttribute(
				IRemoteConnectionConfigurationConstants.ATTR_REMOTE_PATH,
				(String) null);
		String arguments = config.getAttribute(
				ICDTLaunchConfigurationConstants.ATTR_PROGRAM_ARGUMENTS, "");

		AppDebugJob job = new AppDebugJob(device, project, exePath, arguments);
		job.schedule();
		try {
			job.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		DebugPlugin.newProcess(launch, job.getProcess(), "GDB Server Process");
		
		ILaunchConfigurationWorkingCopy wc = config.getWorkingCopy();
		wc.setAttribute(IGDBLaunchConfigurationConstants.ATTR_REMOTE_TCP, true);
		wc.setAttribute(IGDBLaunchConfigurationConstants.ATTR_HOST,
				job.getHostname());
		wc.setAttribute(IGDBLaunchConfigurationConstants.ATTR_PORT,
				Integer.toString(job.getPort()));
		wc.setAttribute(
				ICDTLaunchConfigurationConstants.ATTR_DEBUGGER_START_MODE,
				IGDBLaunchConfigurationConstants.DEBUGGER_MODE_REMOTE);
		wc.doSave();
	}

}
